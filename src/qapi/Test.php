<?php

namespace Quikr\Packagist\QAPI;
/**
 * Created by PhpStorm.
 * User: neerajgupta
 * Date: 5/5/17
 * Time: 5:28 PM
 */
class Test{
    public function __construct(){
        echo "Hello World! This is great";
    }

    public static function printName($name){
        return "You have a nice name : " . $name;
    }
}